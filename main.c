/*Cabecera
\mainpage
\author Mariana Anastasio
\fecha 2020
*/

#include "scheduler.h"
/**\brief Funcion main*/
int main()
{
    /**Nombre del proyecto*/
		printf("\n|-------------------------------------|");
		printf("\n|           � CREAR TAREA �           |");
		printf("\n|------------------|------------------|");


 printf(" \n Cuantas tareas va a crear:");
    scanf("%d",&n);

/**\brief Crea un N numero de tareas*/
for(i=0;i<n;i++)
    {
        /**
        \details
        Le asigna un nuemero al proceso
        \param i es el contador
        \param n es el numero de tareas
        \param p contiene todos los procesos
        \return Retorna O si no se incertan los valores correctos
        */
        printf("\n Tarea[%d]\n",i+1);
        insertarNodo();
        p[i]=i+1;
       }
     desplegarPila();
    return 0;
}

/**\brief  inserta una nueva tarea*/
void insertarNodo(){
    /**
    \details
    Se utiliza el puntero nuevo
    \param prioridad La prioridad para ejecutar la tarea
    \param nombre El nombre del proceso
    */

	nodo* nuevo = (nodo*) malloc(sizeof(nodo));
	printf(" Ingrese una prioridad: ");
	scanf("%d", &nuevo->prioridad);
	printf(" Ingrese el nombre: ");
	scanf("%s", &nuevo->nombre[20]);
	nuevo->siguiente = primero;
	primero = nuevo;
}

/** \brief Muestra todos los elementos de la pila*/

/**
\details
Comprueba el numero de procesos y su prioridad
\param nodo* actual Hace referencia al dato actual de la pila
\param i Numero de procesos
\param prioridad La que tiene la tarea para ejecutarse
*/
void desplegarPila(){
i=1;
	nodo* actual = (nodo*) malloc(sizeof(nodo));
	actual = primero;
	if(primero != NULL){
		while(actual != NULL){
            printf("\n --> En Proceso<--");
			printf("\n El proceso %i tiene una prioridad de %i \n",i++, actual->prioridad);
			actual=actual->siguiente;
	}
	}else{
		printf("\n No hay procesos\n\n");
	}
}

