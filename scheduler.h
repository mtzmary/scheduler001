#ifndef SCHEDULER_H_INCLUDED
#define SCHEDULER_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>

/** Numero de procesos*/
int n;
/** Contador*/
int i;
/** Contiene numero de procesos*/
int p[20];

/** Pila donde se almacenan los datos*/
typedef struct nodo{
    /** Valor de la prioridad*/
    int prioridad;
    /** Nombre de la tarea*/
	char nombre[20];
	/** Puntero que apunta al siguiente*/
	struct nodo* siguiente;
}nodo;
/** Puntero que apunta al primero*/
nodo* primero = NULL;

/**\brief Crea e inserta una nueva tarea*/
void insertarNodo();

/**\brief Muestra todas las tareas*/
void desplegarPila();

#endif // SCHEDULER_H_INCLUDED
